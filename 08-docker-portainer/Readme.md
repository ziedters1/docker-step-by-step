# Managing Docker using [Portainer](https://www.portainer.io/)

Portainer is a lightweight management UI which allows you to easily manage your different Docker environments (Docker hosts or Swarm clusters). Portainer is meant to be as simple to deploy as it is to use. It consists of a single container that can run on any Docker engine (can be deployed as Linux container or a Windows native container, supports other platforms too). There are other UI tools for Docker such as [Kitematic](https://kitematic.com/), [Shipyard](https://shipyard-project.com/), [Rancher](https://rancher.com/), ...

## Installing Portainer on Windows 10

This setup will let you run [Portainer](https://www.portainer.io/) on windows by using the [docker.for.win.localhost](https://docs.docker.com/docker-for-windows/networking/) endpoint.

Please note:

* This will expose your docker API, without TLS, publicly from your machine.
* For more advanced config see the [portainer docs](https://portainer.readthedocs.io/en/latest/deployment.html).

### Step 1 - Enable docker without TLS

Docker settings -> General -> Expose docker daemon on tcp://...

### Step 2 - Run Portainer Image

The only trick here is to use this endpoint: `tcp://docker.for.win.localhost:2375`.

#### Create portainer volume

Powsershell (admin):

```ps
docker volume create portainer_data
```

#### Create portainer container

##### With No Auth

```ps
docker run -d -p 3040:9000 --name portainer --restart=always -v portainer_data:/data portainer/portainer --no-auth -H tcp://docker.for.win.localhost:2375
```

##### With Auth

```ps
docker run -d -p 3040:9000 --name portainer --restart=always -v portainer_data:/data portainer/portainer -H tcp://docker.for.win.localhost:2375
```

Go to http://localhost:3040

### To remove and revert all changes

Powsershell (admin):

```ps
docker stop portainer

docker rm portainer

docker rmi portainer/portainer

docker volume rm portainer_data
```
## Installing Portainer on Linux
- Download the Portainer image from the DockerHub using the docker pull command below.
```shell
$ docker pull portainer/portainer
```
- Next, run the Portainer container with the following command:
```
$ docker run -d -p 9000:9000 --name my_portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v /opt/portainer:/data portainer/portainer
```
- After installing the Portainer container, run the docker ps command to verify the status of the container:

```shell
$ docker ps
```
- Now, open your browser, access the Portainer web interface with your server’s IP or FQDN with port 9000 `http://DOCKER_SERVER_IP:9000/`