# Deploy a Docker Image
---
In this step, we will be pushing our built image to the registry so that we can use it anywhere. The Docker CLI uses Docker’s public registry by default.

## Deployment steps

- Log into the Docker public registry on your local machine.
```shell
$ docker login
```
- Tag the image: It is more like naming the version of the image. It’s optional but it is recommended as it helps in maintaining the version(same like ubuntu:16.04 and ubuntu:17.04)

```shell
$ docker tag my-web-app mromdhani/get-started:01
```
- Publish the image: Upload your tagged image to the repository: Once complete, the results of this upload are publicly available. If you log into Docker Hub, you will see the new image there, with its pull command.
```shell
$docker push  mromdhani/get-started:01
```
That's it, you are done. Now you can go to Docker hub and can check about it also. You published your first image.
![capture](images/publishing.png)





>docker push  mromdhani/get-started:01
The push refers to repository [docker.io/mromdhani/get-started]
aebb2652aa30: Pushed 
7cdbefd28fe4: Pushed
8aa41c245e90: Pushed
668c0ea1e1a4: Pushed
f55aa0bd26b8: Mounted from library/ubuntu
1d0dfb259f6a: Mounted from library/ubuntu
21ec61b65b20: Mounted from library/ubuntu
43c67172d1d1: Mounted from library/ubuntu 
01: digest: sha256:071da23f585371e8c315b3e056103074e4e7d3e12f80348a00c9a4df7efd6120 size: 1985