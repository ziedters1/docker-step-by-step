# Creating a Docker Image
---
Docker makes it easier to create and deploy applications in an isolated environment. A Dockerfile is a script that contains collections of commands and instructions that will be automatically executed in sequence in the docker environment for building a new docker image.

In this step, I will show you how to create your own docker image with a dockerfile. 

## Dockerfile commands
A dockerfile is a script which contains a collection of dockerfile commands and operating system commands (ex: Linux commands).
Below are some dockerfile commands you must know:

- **FROM**
The base image for building a new image. This command must be on top of the dockerfile.

- **MAINTAINER**
Optional, it contains the name of the maintainer of the image.

- **RUN**
Used to execute a command during the build process of the docker image.

- **CMD**
Used for executing commands when we build a new container from the docker image.

- **EXPOSE**
Opens a TCP port.

- **ENV**
Define an environment variable.

- **ARG**
Like ENV bu is only available during the build of a Docker image (RUN etc), not after the image is created and containers are started from it (ENTRYPOINT, CMD).

- **COPY**
Copy a file from the host machine to the new docker image.

- **ADD**
Copy a file from the host machine to the new docker image. There is an option to use a URL for the file, docker will then download that file to the destination directory.

- **ENTRYPOINT**
Define the default command that will be executed when the container is running. This command is not overridable.

    >**RUN vs CMD vs ENTRYPOINT**
     - **RUN** executes command(s) in a new layer and creates a new image. E.g., it is often used for installing software packages.
     - **CMD** sets default command and/or parameters, which can be overwritten from command line when docker container runs.
     - **ENTRYPOINT** configures a container that will run as an executable.

- **WORKDIR**
This is directive for CMD command to be executed.

- **USER**
Set the user or UID for the container created with the image.

- **VOLUME**
Enable access/linked directory between the container and the host machine.

## Creating an Image

### Step 1
Create a folder and  call it myapp — If you are using ubuntu you can run the following commands. Remember to use sudo user if you are a sudo user.
$ mkdir myapp (create our app folder)
$ cd myapp (lets go into our folder)
$ nano Dockerfile (create a file called Dockerfile)
In that docker file paste the following commands
```shell
FROM ubuntu:18.04
MAINTAINER myname <me@mydomain.io>
RUN apt-get update && apt-get install -y apache2 && apt-get clean && rm -rf /var/lib/apt/lists/*
ENV APACHE_RUN_USER  www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR   /var/log/apache2
ENV APACHE_PID_FILE  /var/run/apache2/apache2.pid
ENV APACHE_RUN_DIR   /var/run/apache2
ENV APACHE_LOCK_DIR  /var/lock/apache2
ENV APACHE_LOG_DIR   /var/log/apache2
RUN mkdir -p $APACHE_RUN_DIR
RUN mkdir -p $APACHE_LOCK_DIR
RUN mkdir -p $APACHE_LOG_DIR
COPY index.html /var/www/html
EXPOSE 80
CMD ["/usr/sbin/apache2", "-D", "FOREGROUND"]
```
In the same root folder lets create a file called `index. html`. Copy the following in to the index.html page
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>First APP</title>
</head>
<body>
    <h1>I am served by a Dockerized app ...</h1>
</body>
</html>
```
  > -D FOREGROUND When -D FOREGROUND is not passed apache is run in the background and the apache process will be detached from the shell. This will cause docker container to be stopped.

### Step 2
After we have completed our configuration then we can now run the command to build our image.
```shell
# Build an image using the Dockerfile at current location
# Example: docker build -t [name] .

$ docker build -t my-web-app .
```
The name of the image is `my-web-app` and the `.` operator specifies the current directory which tells docker to look through the current directory for a Dockerfile.
The `-t `flag here is used to tag the image. You can use `docker build --help`. 

### Step 3
Now we have our image built with our dockerfile. Using the image we have built, we can now proceed to the final step: creating a container running a Apache instance inside, using a name of our choice (if desired with -name [name]).
```shell
$docker run --name my_first_apache_instance -i -t my-web-app
```
   > When you see a message like this , ignore it
     `Note: If a name is not set, we will need to deal with complex, alphanumeric IDs which can be obtained by listing all the containers using docker ps -a`

Now lets run docker ps and we should see our container built already 
my_first_`apache`_instance Is given as the container name.
To run our container on port 80 of our host so that we can see the output of our index.html, run the command below; 80:80 maps port 80 on the container to port 80 on the host. and if your containerized application runs on port 3000, we would expose it to our host port like so 3000:3000
```shell
#Usage docker run -d -p 80:80 image name
#in our case 
docker run -p 80:80 my-web-app
#we can use 
docker run -d -p 80:80 my-web-app
# -d flag currently enables it to run in the background.
Then head over to your browser and run localhost
```
![capture](images/custominage.png)
