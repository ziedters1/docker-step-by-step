# Docker containers Management
---
Docker runs processes in isolated containers. A container is a process which runs on a host. The host may be local or remote.
In this step, we will learn how to run, to stop, to remove containers.

# Running a Container
Running of containers is managed with the Docker `run` command.
The basic docker run command takes this form:

```shell 
$ docker run [OPTIONS] IMAGE[:TAG|@DIGEST] [COMMAND] [ARG...]
```
The docker `run` command must specify an IMAGE to derive the container from. An image developer can define image defaults related to:

- detached or foreground running
- container identification
- network settings
- runtime constraints on CPU and memory

With the `docker run [OPTIONS]` an operator can add to or override the image defaults set by a developer. And, additionally, operators can override nearly all the defaults set by the Docker runtime itself. The operator's ability to override image and Docker runtime defaults is why run has more options than any other docker command.

To learn how to interpret the types of `[OPTIONS]`, see [Option types](https://docs.docker.com/engine/reference/commandline/cli/#option-types).

To run a container , first launch the Docker container.

```shell
$ docker container run ubuntu echo "bonjour le monde !"
Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
5c939e3a4d10: Pull complete
c63719cdbe7a: Pull complete
... 
Digest: sha256:8d31dad0c58f552e890d68bbfb735588b6b820a46e459672d96e585871acc110
Status: Downloaded newer image for ubuntu:latest
bonjour le monde !
```
We created and executed our container, but since it did not find the ubunto image locally, it downloaded it by itself (without having to use docker image pull). Then it has executed the command we placed with him, namely to write "bonjour le monde !". And it's all, since the echo is finished, he turned off the container.
We will now check if this container is started or not, to do this we will use docker container ls:
```shell
$ docker container ls
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```
We don't see any container. If we add the `-a` option we can see all the containers.
```shell
$ docker container ls -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                     PORTS               NAMES
ae198d73aafc        ubuntu              "echo 'bonjour ..."   4 minutes ago       Exited (0) 4 minutes ago                       pedantic_snyder
```
Here it is, small explanation of this table:

- _CONTAINER_ ID: Container ID, generated so that it is unique
- _IMAGE_: The image used for this container
- _COMMAND_: The command executed
- _CREATED_: Time since container creation
- _STATUS_: The current status of the container, here exited with a return code 0 (without error) for 4 minutes
- _PORTS_: List of listened ports (we will see this later)
- _NAMES_: Name of the container, here it is a random name because we have not defined one to our container

**Create a container with a name**
You can use _-h_ command line parameter to specify a container name.

```
$ docker run -h CONTAINER1 alpine /bin/sh
```
## Running a container in an interactive mode

```shell
$ docker run –it ubuntu /bin/bash 
```
- _-i (interactive)_ is about whether to keep stdin open (some programs, like bash, use stdin and other programs don't). -d (detached) is about whether the docker run command waits for the process being run to exit. Thus, they are orthogornal and not inherently contradictory. A program like bash exits when stdin in closed, so without -i, it exits immediately.

- _-t allocates a pseudo-tty_. You can see the difference from running bash with -it vs with just -i. For example, without -t, you don't get any prompt and ls shows results in one column. This difference is like the difference between running ls and running ls | cat, where cat does not have a pseudo-tty.

**What is the detached mode ?**
Detached mode, shown by the option --detach or -d, means that a Docker container runs in the background of your terminal. It does not receive input or display output.

```
docker run -d IMAGE
```
If you run containers in the background, you can find out their details using docker ps and then reattach your terminal to its input and output.


## List of Docker containers running
```
$ docker container ls -a
```
or 
```
$ docker ps -a
```

## Inspect a Container
```
$ docker inspect CONTAINER_NAME
```
Output will be a JSON file.

## Stop a Running Container
```shell
$ docker stop [CONTAINER_NAME]
```
## Start a Stopped Container
```shell
$ docker start [CONTAINER_NAME]
where [CONTAINER_NAME] is the container name.
```
## Enter the Shell of a Started Container

```shell
$ docker attach CONTAINER_NAME
```
where CONTAINER_NAME is the container name.

## Delete a Container

```shell
docker rm CONTAINER_NAME
```

## Detach from a Container
```shell
docker run -t -i → can be detached with ^P^Q and reattached with 
docker attach
docker run -i    → cannot be detached with ^P^Q; will disrupt stdin
docker run       → cannot be detached with ^P^Q; can SIGKILL client; can reattach with docker attach
```                   


## Docker Logs
If you run this command with the name of your container, you should get a list of commands executed in the container.

```
$ docker logs CONTAINER_NAME
where hopeful_pare is the container name.
```
## Pause a Container
```shell
$ docker pause CONTAINER_NAME
```
Note : Container needs to be in the Started Phase

## UnPaise a Paused Container
```shell
$ docker unpause CONTAINER_NAME
```

## Remove all stopped containers
Before performing the removal command, you can get a list of all non-running (stopped) containers that will be removed using the following command:

```shell
docker container ls -a --filter status=exited --filter status=created 

```
To remove all stopped containers use the docker container prune command:

```shell
docker container prune

```
You'll be prompted to continue, use the -f or --force flag to bypass the prompt.

## Removing all the Containers
```shell
$ docker container rm $(docker container ls -aq)
```
## Rename a Docker Container
```shell
$ docker rename big_hamilton big_hamilton_v1
```