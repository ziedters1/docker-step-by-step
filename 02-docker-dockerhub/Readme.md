## Docker Hub
---

## What is Docker Hub ?
Docker Hub is a registry service on the cloud that allows you to download Docker images that are built by other communities. The base images were created by the docker team. You can also upload your own Docker built images to Docker hub. There are two different types of images:

- Official images;
- Personal images.

It can be accessed here: https://hub.docker.com/explore/
In this step, we will see how to download and the use the Jenkins Docker image from Docker hub.

## Finding images on Docker Hub

If you are looking for an image, there are two ways to do it:

- the web interface;
- the command line.

To search via the web interface, type in the name of the image in the search input text. The following figure shows the search results for the jenkins image. You will see the number of downloads and the stars for the each image.
![capture](images/webinterface.png).

To get more details on a given image, click on it. These are the details about the offical Jenkins image. Note that the jenkins image havs several tags.
![capture](images/jenkinsimage.png)


To search via the command line use the `docker search` command. The following figure shows the search results for the jenkins image.

```shell
C:\Users\m.romdhani>docker search jenkins
NAME                                   DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
jenkins                                Official Jenkins Docker image                   4607                [OK]
jenkins/jenkins                        The leading open source automation server       1866
jenkinsci/blueocean                    https://jenkins.io/projects/blueocean           479
jenkinsci/jenkins                      Jenkins Continuous Integration and Delivery …   376
jenkinsci/jnlp-slave                   A Jenkins slave using JNLP to establish conn…   117                                     [OK]
jenkins/jnlp-slave                     a Jenkins agent (FKA "slave") using JNLP to …   110                                     [OK]
jenkinsci/slave                        Base Jenkins slave docker image                 62                                      [OK]
jenkinsci/ssh-slave                    A Jenkins SSH Slave docker image                40                                      [OK]
jenkins/slave                          base image for a Jenkins Agent, which includ…   38                                      [OK]

```
## Pulling and Running an image
Now, let's pull the docker image with the `docker pull` command
```shell
C:\Users\m.romdhani>docker pull jenkins
Using default tag: latest
latest: Pulling from library/jenkins
55cbf04beb70: Pull complete  
1607093a898c: Pull complete 
9a8ea045c926: Pull complete
d4eee24d4dac: Pull complete
c58988e753d7: Pull complete
794a04897db9: Pull complete 
70fcfa476f73: Pull complete
0539c80a02be: Pull complete
54fefc6dcf80: Pull complete
911bc90e47a8: Pull complete
38430d93efed: Pull complete 
7e46ccda148a: Pull complete
c0cbcb5ac747: Pull complete
35ade7a86a8e: Pull complete 
aa433a6a56b1: Pull complete
841c1dd38d62: Pull complete 
b865dcb08714: Pull complete 
5a3779030005: Pull complete
12b47c68955c: Pull complete
1322ea3e7bfd: Pull complete
Digest: sha256:eeb4850eb65f2d92500e421b430ed1ec58a7ac909e91f518926e02473904f668
Status: Downloaded newer image for jenkins:latest
docker.io/library/jenkins:latest
```
To run Jenkins, you need to run the following command:
```shell
docker run -p 8080:8080  jenkins
```