# Docker Images Management
A Docker image is a snapshot, or template, from which new containers can be started. It’s a representation of a filesystem plus libraries for a given OS. Images and containers are not the same—a container is a running instance of an image. A single image can be used to start any number of containers. Images are read-only, while containers can be modified, Also, changes to a container will be lost once it gets removed, unless changes are committed into a new image.

Docker CLI offers a specific category for managing images:  `docker image [subcommand]` :
```shell
$ docker image --help

Usage:  docker image COMMAND

Manage images

Options:
      --help   Print usage

Commands:
  build       Build an image from a Dockerfile
  history     Show the history of an image
  import      Import the contents from a tarball to create a filesystem image
  inspect     Display detailed information on one or more images
  load        Load an image from a tar archive or STDIN
  ls          List images
  prune       Remove unused images
  pull        Pull an image or a repository from a registry
  push        Push an image or a repository to a registry
  rm          Remove one or more images
  save        Save one or more images to a tar archive (streamed to STDOUT by default)
  tag         Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE

Run 'docker image COMMAND --help' for more information on a command.
```

## Download (Pull) Docker images

To downloadan image we use the floowing command:
```shell
$ docker image pull [nom image]:[tag]
```

Ce qui donne pour télécharger notre owncloud :
```shell
$ docker image pull maven
Using default tag: latest
latest: Pulling from library/maven
1a267c67f42: Pull complete
70377701f89: Pull complete
55c73a122bc: Pull complete
b71bac61c47: Pull complete
88a1d91ad4e: Pull complete
6e0256ba4b0: Pull complete
14fbe7a9dfb: Pull complete
f36dd91c0ab: Pull complete
2e4a1f87acc: Pull complete
a5541ee478f: Pull complete

Digest: sha256:br5d3f4feeacedfb936b802a5c05885db3abcbb909315aed162c2d8938f4ab29
Status: Downloaded newer image for maven:latest
```

If you don't put a tag, the CLI automatically downloads the `latest` tag.
As we saw in the section on the dockerhub, an image may have several tags.
By specifying a tag, for example  3.6-jdk-8, 3-jdk-8 it would give:

```shell
$ docker image pull maven:3.6-jdk-8
3.6-jdk-8: Pulling from library/maven
70a267c67f42: Already exists
070377701f89: Already exists
...
```

An image is often based on another image, which can be based on another and so on. These are layers. You will understand this better when we learn how to create images. Each layer has a unique id, which makes it possible to know if it is already present or not.

On certain images, like the official ones, several tags can be associated with the same image for the same version, for example we can see on the hub, that 3.6.3-jdk-8, 3.6-jdk-8, 3-jdk-8 are equivalent tags. 

## Image variants
An image can have several variants which are designed for a specific use case. Variants are visible in the image description page on Docker Hub site. Maven images, for example, are provided in these varaiants:

- _maven: \<version>_
This is the defacto image. If you are unsure about what your needs are, you probably want to use this one. It is designed to be used both as a throw away container (mount your source code and start the container to start your app), as well as the base to build other images off of.

- _maven:\<version>-slim_
This image does not contain the common packages contained in the default tag and only contains the minimal packages needed to run maven. Unless you are working in an environment where only the maven image will be deployed and you have space constraints, we highly recommend using the default image of this repository.

- _maven:\<version>-alpine_
This image is based on the popular Alpine Linux project, available in the alpine official image. Alpine Linux is much smaller than most distribution base images (~5MB), and thus leads to much slimmer images in general.

## List images
To list the downloaded images, therefore available locally, we will use this command:
```shell
$ docker image ls
REPOSITORY                    TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
```

* REPOSITORY : The image name
* TAG : The image version
* IMAGE ID : Unique ID of the image
* CREATED : Creation data of the image
* VIRTUAL SIZE : Image size + size of its dependencies.

```shell
$ docker image ls
REPOSITORY             TAG                 IMAGE ID            CREATED             SIZE
mromdhani/cheers2019   latest              7c3481f74ff7        22 hours ago        4.01MB
maven                  latest              49e6e5e8cdfa        3 days ago          617MB
maven                  3.6-jdk-8           ca9e492e201d        3 weeks ago         499MB
golang                 1.11-alpine         e116d2efa2ab        5 months ago        312MB
jenkins                latest              cd14cecfdb3a        18 months ago       696MB
```
You can also display only the desired image (s)
```shell
$ docker images maven
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
maven               latest              49e6e5e8cdfa        3 days ago          617MB
maven               3.6-jdk-8           ca9e492e201d        3 weeks ago         499MB
```

If you don't remember the full name anymore, we can play around with the regex:

```shell
$ docker images *mav*
REPOSITORY            TAG                 IMAGE ID            CREATED             SIZE
maven               latest              49e6e5e8cdfa        3 days ago          617MB
maven               3.6-jdk-8           ca9e492e201d        3 weeks ago         499MB
```

## Remove images
To move an image, use the rm subcommand:
```shell
$ docker image rm [ Image name ou ID ]:[tag]
```

Here is and exemple :
```shell
$ docker image rm maven:latest
Untagged: maven:latest
Untagged: maven@sha256:9d47211ca3cdd57a60911726cae70caa4e6401b35c4e27c6ea4197bdaf16a293
Deleted: sha256:49e6e5e8cdfa3f401e1805e100700bc90671e461914691a59eff4488a7cc9827
Deleted: sha256:c3afaae97b95dda27dc9195bc4bb19d0d8a9ea3324c100dd3989813b8c56e125
Deleted: sha256:ed31d2e607c06e65d97ecbeedb0057880bc788d4df89f95980fe548a3028e155
Deleted: sha256:9b134c49a318fa83c9b2b322879dc6a2c21da4ab6a1f174ec359bfa1bbe87474
Deleted: sha256:1872bc9c598e99800320d9ff9508ca0b7590d4bb2855a718d855e92806ec122e
Deleted: sha256:90842fedc1c997a4d4db5b5ce70e8773ab28ed1742408945101dea3b0eadfc67
```

You can also remove multiple image in a the command:
```shell
$ docker image rm [image] [image] [image]
```

  > **Troubleshooting**
    When you try to relve an image 
    `docker image rm 75835a67d134` 2a4cca5ac898
    You may get an error similar to the one shown below:
    `Error response from daemon: conflict: unable to remove repository reference "centos" (must force) - container cd20b396a061 is using its referenced image 75835a67d134`µ
    This means that **an existing container uses the image**. To remove the image, you will have to **remove the container first**.

  * **Remove dangling images**
Docker provides a docker `image prune` command that can be used to remove **dangled** and unused images. A dangling image is an image that is not tagged and is not used by any container. To remove dangling images type:

    ``` shell
    $ docker image prune
    You ll be prompted to continue, use the -f or --force flag to bypass the prompt.
    WARNING! This will remove all dangling images.
    Are you sure you want to continue? [y/N] y
    ```

* **Remove all unused images**
To remove all images which are not referenced by any existing container, not just the dangling ones, use the prune command with the -a flag:

  ```shell
  $ docker image prune -a
  ```