# Sharing volumes
---
A Docker volume is a directory (or collection of files) that lives on the host file system and is not a part of the container's UFS. It is within these volumes that containers are capable of saving data. With the docker volume command, you can easily manage volumes to expand your containers well beyond their basic capability.

Let's find out how to deploy a new container that includes a volume attached to a specific directory on a host such that the container will stay in sync with the data in the volume. 

## Creating the Host Data Volume
The first step is to create a new directory to house the volume. To do this, open a terminal window and issue the command:

```shell
$ mkdir C:\tmp
$ cd tmp
$ mkdir container-data
```
You must ensure the newly-created directory is housed in a location the Docker user can access (with read-write privilege).

Once you've created that directory, you're ready to mount a volume inside. Let's say you're going to deploy a container, based on the official Ubuntu image, that contains a directory called /data. To deploy such a container that attaches the internal /data directory to a volume within the host directory ~/container-data, you would issue the command:

```shell
$ docker run -dit -P --name ubuntu-test -v C:\tmp\container-data:/data ubuntu:18.04
```
The above command breaks down like this:

docker run is the main command that says we’re going to run a command in a new container.
- _-dit _is d for detached mode, and it ensures that bash or sh can be allocated to a pseudo terminal.
- _-P_ publishes the containers ports to the host.
- _–name_ says what follows is the name of the new container.
- **_-v_** says what follows is to be the volume.
ubuntu:18.04 is the image to be used for the container.
Make sure to remember the first  characters of that ID, as you'll need it to gain access to the container bash prompt.
Access the newly-deployed container with the command:
```shell
$ docker attach 5cdea
```
Issue the command `ls / `and you will see the `/data` directory added to the Ubuntu container. Let's create a test file in that directory with the command:

```shell
$touch /data/test
```
After creating this test file, open another terminal window on the host machine and issue the command dir `C:\tmp\container-data`. You should see the test file within that directory.

# Database Volumes
Let's say you want to create a volume for a database. You can do this by first deploying a MySQL database Docker container and instructing it to use a persistent storage volume named mysql-data. Do this with the command:

```shell
$ docker run --name mysql-test -v mysql-data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=passwd -d mysql:latest
```
In the above command, the -e switch informs docker what follows is an environment variable.

Access the bash prompt for the container with the command:

```shell
$ docker exec -it ID /bin/bash
```
Where ID is the first four characters of the ID of the newly deployed container.

List out the contents of the container’s /var/lib/mysql directory with the command:

```shell
$ ls /var/lib/mysql
```
Make note of those contents and exit from the container with the command:

```shell
$ exit
```
Now, check the contents of the host’s mounted volume with the command:

``` shell
$ sudo ls /var/lib/docker/volumes/mysql-data/_data
```
You now have a MySQL-based container which includes persistent storage mounted in a volume on a host computer.
